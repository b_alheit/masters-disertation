import matplotlib.pyplot as plt
import numpy as np

strain_original = np.array([0, 0.21, 0.43, 0.608])
stress_original = 1000 * np.array([0, 1.5, 5.1, 6.4])

modulus_original = (stress_original[2] - stress_original[1]) / (strain_original[2] - strain_original[1])

modulus_new = 0.6e9
strain_f_new = 0.8

sf_vert = modulus_new / modulus_original
sf_hor = strain_f_new/strain_original[-1]

# sf_vert = 1
# sf_hor = 1

strain_new = sf_hor * strain_original
stress_new = sf_vert * stress_original

c0 = np.zeros(3)
c1 = np.zeros(3)
c2 = np.zeros(3)

c0[0] = (modulus_new - stress_new[1]/strain_new[1]) / strain_new[1]
c0[1] = modulus_new - 2 * c0[0] * strain_new[1]

c1[1] = modulus_new
c1[2] = strain_new[1] - strain_new[1] * modulus_new

c2[0] = modulus_new/(2*(strain_new[2] - stress_new[3]))
c2[1] = - 2 * c2[0] * strain_new[3]
c0[2] = stress_new[3] - (c2[0] * strain_new[3] ** 2 + c2[1] * strain_new[3])

x = np.linspace(0, strain_new[-1], 1000)
y = np.zeros(1000)

def find_y(x):
	if x < strain_new[1] : 
		c = c0
	elif strain_new[1] <= x <= strain_new[2]:
		c = c1
	elif strain_new[2] < x <= strain_new[3]:
		c = c2
	return c[0] * x **2 + c[1] * x + c[2]

for i in range(len(x)):
	y[i] = find_y(x[i])

print(stress_new)
print(strain_new)

print(find_y(0))
print(find_y(strain_new[1]))
print(find_y(strain_new[2]))
print(find_y(strain_new[3]))

plt.plot(x, y)
plt.show()
