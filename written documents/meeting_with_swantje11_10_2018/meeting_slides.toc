\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\beamer@endinputifotherversion {3.36pt}
\defcounter {refsection}{0}\relax 
\select@language {english}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {2}{Introduction}{3}{0}{1}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {3}{Turtle shell structure}{4}{0}{2}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {3}{1}{The Carapace}{5}{0}{2}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {3}{2}{Bone in turtle shell}{6}{0}{2}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {3}{3}{Sutures and collagenous soft tissue in turtle shell}{9}{0}{2}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {3}{4}{Keratinous hard tissue in turtle shell}{11}{0}{2}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {4}{Modelling idealised suture tooth}{13}{0}{3}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {4}{1}{Idealised model}{14}{0}{3}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {4}{2}{Linear elastic results}{15}{0}{3}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {4}{3}{Linear elastic-plastic results}{17}{0}{3}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {4}{4}{Hyper-elastic Yeoh model for collagen and linear elastic bone}{18}{0}{3}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {4}{5}{Issues implementing other material models}{20}{0}{3}
